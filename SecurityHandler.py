from pyftpdlib.handlers import FTPHandler

from UserAuthorizer import UserAuthorizer
from async_cypher import decipher_blocking, AsyncCypher, Cypher
from threading import Event
import string
import random
import os
from os import listdir


class SecurityHandler(FTPHandler):
    client_home_dir_state = dict()
    client_logged = dict()

    def __init__(self, conn, serv, ioloop):
        super().__init__(conn, serv, ioloop)
        self.state_valid_event = Event()

    def _handle_new_connection(self, username):
        """
        This method should block until the directory is confirmed to be in valid state

        :param username:  new user logging to the server
        :return:
        """
        if not self.client_home_dir_state[username]:
            self.state_valid_event.wait()
        self.client_logged[username] = True

    def on_login(self, username):
        self._handle_new_connection(username)
        decipher_blocking(Cypher(file_name=self.authorizer.get_passphrase_file_name(username),
                                 directory=self.authorizer.get_home_dir(username)))

    def on_logout(self, username):
        pass

    def on_disconnect(self):
        username = self.username
        if self.authenticated and username:
            self.state_valid_event.clear()

            def callback():
                self.client_home_dir_state[username] = True
                self.client_logged = False
                self.state_valid_event.set()

            async_decipher = AsyncCypher(callback, self.get_cypher(username))
            async_decipher.start()
        else:
            print("We are officially screwed")

    @classmethod
    def get_cypher(cls, username):
        return Cypher(file_name=cls.authorizer.get_passphrase_file_name(username),
                      directory=cls.authorizer.get_home_dir(username))

    @classmethod
    def on_move_event(cls):
        """
        This method attempt to make user data unaccesible as soon as possible. It's actions are:
        1. Overwrite contents of all files in users folder with random data, and remove them
        2. umount all directories of all logged in users
        :return:
        """

        for file in listdir("users/"):
            cls.shred_file("users/" + file)
        for cypher in [cls.get_cypher(x) for x, logged in cls.client_logged.items() if logged]:
            cypher.cypher()

    @classmethod
    def add_user(cls, username, password, homedir):
        assert isinstance(cls.authorizer, UserAuthorizer)
        cls.authorizer.add_user(username, password, homedir)
        cls.client_home_dir_state[username] = True

    @classmethod
    def restore_users(cls):
        assert isinstance(cls.authorizer, UserAuthorizer)
        for username in cls.authorizer.read_users():
            cls.client_home_dir_state[username] = True

    @staticmethod
    def shred_file(file_name, passes=1):
        def generate_data(length):
            characters = string.ascii_lowercase + string.ascii_uppercase + string.digits
            return ''.join(random.SystemRandom().choice(characters) for _ in range(length))

        if not os.path.isfile(file_name):
            return

        l = os.path.getsize(file_name)
        with open(file_name, "w") as f:
            for _ in range(int(passes)):
                data = generate_data(l)
                f.write(data)
                f.seek(0, 0)

        os.remove(file_name)
