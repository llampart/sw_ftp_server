import re
from threading import Thread

from SecurityHandler import SecurityHandler


class ClientHandler(Thread):
    help_pattern = r"\s*-h\s*"
    add_user_pattern = r"\s*-u\s*"
    remove_user_pattern = r"\s*-d\s*"

    def __init__(self):
        super().__init__(target=self.main_loop)

    def add_user(self, username: str, password: str, home_dir: str):
        SecurityHandler.add_user(username, password, home_dir)

    def remove_user(self, username: str):
        pass

    def print_options(self):
        print('Type -h to get list of all available comments.')
        print('Type -u to add new user, you will be asked for needed information.')
        print('Type -r to delete existing user, you will be asked for needed information.')

    @classmethod
    def get_command(cls, command: str):
        if re.match(cls.help_pattern, command) is not None:
            return cls.print_options
        elif re.match(cls.add_user_pattern, command) is not None:
            return cls.add_user_input
        elif re.match(cls.remove_user_pattern, command) is not None:
            return cls.remove_user_input
        elif re.match(r"\s*", command) is not None:
            def empty_func(x):
                pass

            return empty_func
        else:
            return lambda x: print("Wrong option, type -h to get all possible commands")

    def add_user_input(self):
        try:
            username = input("Type user login: ")
            password = input("Type user password: ")
            home_dir = input("Type user home directory path: ")
            self.add_user(username, password, home_dir)
            print("Successfully added new user")
        except Exception:
            print("Failed to add user, try again")

    def remove_user_input(self):
        try:
            username = input("Type user login")
            self.remove_user(username)
        except Exception:
            print("Failed to remove user, check if the login is correct")

    def main_loop(self):
        while True:
            command_string = input("Enter your command here (type -h to get the list of all available commands): ")
            command = ClientHandler.get_command(command_string)
            command(self)
