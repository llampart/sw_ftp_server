from SecurityHandler import SecurityHandler
from SimpleIntegral import integrate
from schedule_utils import with_schedule, with_finite_schedule
import sched, time
import os

from LIS3DH import LIS3DH


class KillWorker:
    """
    Worker attempts to detect movement of the device (calculate velocity).
    It can be described as a state machine with the following semantics:
     Creation -[call to initialization method]-> PositionInitialization -[2 seconds of acceleration measurements]->
     Movement detection
    While in "Movement detection" state any significant movement of the device should be registered and proper callback
    invoked. Important note, device should react to it's movement as quickly as possible so good adjustments to
    constants may be necessary in some cases.
    """

    period = 60
    interval = 0.0005

    def __init__(self, handler: SecurityHandler, velocity_threshold=0.002):
        self.handler = handler
        self.scheduler = sched.scheduler(time.time)
        self.velocity_threshold = velocity_threshold
        self.velocity_list = list()
        self.position_list = list()
        self.sensor = None
        self.max_size = 5
        # here we have to somehow remove noise (those lambdas)
        self.static_x_position = 0
        self.static_y_position = 0
        self.static_z_position = 0
        self._average_positions = []
        self.scheduler = sched.scheduler()
        self.deleted = False

    # alternative approach, i think more readable, method which we "integrate"
    # Have to find how to cut those "small accelerations", this method may catch them and return some speed
    @integrate(interval, period)
    def x_acceleration(self):
        return self.sensor.getX() - self.static_x_position

    @integrate(interval, period)
    def y_acceleration(self):
        return self.sensor.getY() - self.static_y_position

    @integrate(interval, period)
    def z_acceleration(self):
        return self.sensor.getZ() - self.static_z_position

    def init_security_handling(self):
        sensor = LIS3DH(debug=True, bus=1)
        sensor.setRange(LIS3DH.RANGE_2G)
        self.sensor = sensor
        self.initialize_position()
        self.scheduler.enter(2, 1, self.run_with_position_initialization, (lambda: self.handle_movement_alt(),))
        self.scheduler.run()

    def init_security_handling_test(self):
        sensor = LIS3DH(debug=True, bus=1)
        sensor.setRange(LIS3DH.RANGE_2G)
        self.sensor = sensor
        self.initialize_position()
        self.scheduler.enter(2, 1, self.run_with_position_initialization,
                             (lambda: self.handle_movement_test_alt(),))
        self.scheduler.run()

    def run_with_position_initialization(self, fun):
        self.static_x_position = sum(map(lambda x: x[0], self._average_positions)) / len(self._average_positions)
        self.static_y_position = sum(map(lambda x: x[1], self._average_positions)) / len(self._average_positions)
        self.static_z_position = sum(map(lambda x: x[2], self._average_positions)) / len(self._average_positions)
        print(len(self._average_positions))
        print("Position: ", self.static_x_position, self.static_y_position, self.static_z_position)
        fun()

    @staticmethod
    def calculate_velocity_vector(x_v, y_v, z_v):
        return (x_v * x_v + y_v * y_v + z_v * z_v) ** 0.5

    @with_schedule(interval)
    def handle_movement_test_alt(self):
        if self.calculate_velocity_vector(self.x_acceleration(), self.y_acceleration(),
                                          self.z_acceleration()) > self.velocity_threshold:
            print("Possible movement! Estimated velocity:",
                  self.calculate_velocity_vector(self.x_acceleration(), self.y_acceleration(), self.z_acceleration()))

    @with_schedule(interval)
    def handle_movement_alt(self):
        if self.calculate_velocity_vector(self.x_acceleration(), self.y_acceleration(),
                                          self.z_acceleration()) > self.velocity_threshold:
            if not self.deleted:
                print("Movement! Deleting...")
                self.handler.on_move_event()
                self.deleted = True
                os._exit(1)


    @with_finite_schedule(0.001, 2000)
    def initialize_position(self):
        self._average_positions.append((self.sensor.getX(), self.sensor.getY(), self.sensor.getZ()))

    def add_velocity(self, x, y, z):

        def calculate_velocity(xs: tuple, ys: tuple, zs: tuple):
            return abs(xs[1] - xs[0]) + abs(ys[1] - ys[0]) + abs(zs[1] - zs[0])

        is_critical = False

        if len(self.position_list) == 0:
            self.position_list.append((x, y, z))
            self.velocity_list.append(0)
        else:
            old_x, old_y, old_z = self.position_list[-1]
            self.position_list.append((x, y, z))
            velocity = calculate_velocity((old_x, x), (old_y, y), (old_z, z))
            self.velocity_list.append(velocity)
            is_critical = self.test_speed()
        if len(self.position_list) > self.max_size:
            self.position_list = self.position_list[1:]
            self.velocity_list = self.velocity_list[1:]
        return is_critical

    def test_speed(self):
        return max(self.velocity_list) > self.velocity_threshold
