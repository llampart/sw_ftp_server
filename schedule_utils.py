from threading import Timer


def with_schedule(delay):
    def with_schedule_decorator(func):
        def func_wrapper(*args, **kwargs):
            func(*args, **kwargs)
            Timer(delay, func_wrapper, args, kwargs).start()

        return func_wrapper

    return with_schedule_decorator


def with_finite_schedule(delay, repeat):
    def with_schedule_decorator(func):
        counter = 0

        def func_wrapper(*args, **kwargs):
            func(*args, **kwargs)
            nonlocal counter
            if counter < repeat:
                counter += 1
                Timer(delay, func_wrapper, args, kwargs).start()

        return func_wrapper

    return with_schedule_decorator
