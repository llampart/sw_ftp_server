import os

from pyftpdlib.servers import ThreadedFTPServer
from KillWorker import KillWorker
from ClientHandler import ClientHandler
from SecurityHandler import SecurityHandler
from UserAuthorizer import UserAuthorizer


def main(debug: bool):
    # Instantiate a dummy authorizer for managing 'virtual' users
    authorizer = UserAuthorizer()
    # Define a new user having full r/w permissions and a read-only
    # anonymous user
    # authorizer.add_user('user', '12345', '.', perm='elradfmwM')
    authorizer.add_anonymous(os.getcwd())
    # Instantiate FTP handler class
    handler = SecurityHandler
    handler.authorizer = authorizer

    # Define a customized banner (string returned when client connects)
    handler.banner = "pyftpdlib based ftpd ready."
    # Restore users
    handler.restore_users()

    # Initialize accelerometer
    kill_worker = KillWorker(handler)
    if debug:
        kill_worker.init_security_handling_test()
    else:
        kill_worker.init_security_handling()

# Specify a masquerade address and the range of ports to use for
    # passive connections.  Decomment in case you're behind a NAT.
    # handler.masquerade_address = '151.25.42.11'
    # handler.passive_ports = range(60000, 65535) user1234

    # Instantiate FTP server class and listen on 0.0.0.0:2121
    address = ('', 21)
    server = ThreadedFTPServer(address, handler)

    # set a limit for connections
    server.max_cons = 256
    server.max_cons_per_ip = 5

    # start ftp server
    client_handler = ClientHandler()
    client_handler.start()
    server.serve_forever()
