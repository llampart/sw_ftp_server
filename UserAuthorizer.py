import os
from os.path import isfile, join, exists
import sys
from hashlib import sha256
import re

from pyftpdlib.authorizers import DummyAuthorizer, AuthenticationFailed


class UserAuthorizer(DummyAuthorizer):
    users_files_directory = "users/"
    users_home_dir_repository = "home_directories/"

    def get_user_phash(self, username: str):
        return self.user_table[username]['pwd']

    def add_user(self, username, password, homedir, perm='elr', msg_login="Login successful.", msg_quit="Goodbye."):
        if sys.version_info >= (3, 0):
            password = password.encode('latin1')
        password_hash = sha256(password).hexdigest()

        with open(self.get_passphrase_file_name(username), "w+") as file:
            file.write("passphrase_passwd={}".format(password_hash))

        with open(self.get_misc_data_file_name(username), "w+") as file:
            file.write("home_dir={}".format(homedir))

        return super().add_user(username, password_hash, homedir, perm, msg_login, msg_quit)

    def restore_user(self, username, password_hash, homedir, perm='elr', msg_login='Login successful.',
                     msg_quit='Goodbye.'):
        return super().add_user(username, password_hash, homedir, perm, msg_login, msg_quit)

    def get_passphrase_file_name(self, username):
        return "{0}/{1}.txt".format(self.users_files_directory, username)

    def get_misc_data_file_name(self, username):
        return "{0}/{1}.txt".format(self.users_home_dir_repository, username)

    def validate_authentication(self, username, password, handler):
        if sys.version_info >= (3, 0):
            password = password.encode('latin1')
        password_hash = sha256(password).hexdigest()
        try:
            if self.get_user_phash(username) != password_hash:
                raise KeyError
        except KeyError:
            raise AuthenticationFailed

    def read_users(self):
        """
        This method provides iterator for restored users (in order for security handler to work)
        For a user to be restored properly it has to have it's password and home_dir defined in proper directories,
        in files named {username}.txt
        :return:
        """
        if not exists(self.users_files_directory):
            os.mkdir(self.users_files_directory)

        if not exists(self.users_home_dir_repository):
            os.mkdir(self.users_home_dir_repository)

        for file_parts in [f for f in os.listdir(self.users_files_directory) if
                           isfile(join(self.users_files_directory, f))]:
            username = file_parts.split(".")[0]
            with open(join(self.users_files_directory, file_parts)) as file:
                password_hash_lookup = re.search(r"passphrase_passwd=(.*)", file.readline())
                password_hash = password_hash_lookup.group(1)
            with open(join(self.users_home_dir_repository, file_parts)) as file:
                home_dir_lookup = re.search(r"home_dir=([a-zA-Z0-9/]*)", file.readline())
                home_dir = home_dir_lookup.group(1)
                print(home_dir)
            if password_hash and home_dir:
                self.restore_user(username, password_hash, home_dir)
            yield username
