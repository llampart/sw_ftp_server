import os
import subprocess
from threading import Thread


class Cypher:
    def __init__(self, directory: str, file_name=None):
        self.file_name = file_name
        self.directory = os.path.abspath(directory)
        self.key_bytes = 16
        self.cipher_type = 'aes'

    def decipher(self):
        m = 'passphrase_passwd_file'
        command = ["mount", "-t", "ecryptfs", "-o",
                   "key=passphrase,{}={},ecryptfs_cipher={},ecryptfs_key_bytes={},ecryptfs_passthrough=n,verbosity=0"
                       .format(m, self.file_name, self.cipher_type, self.key_bytes),
                   self.directory, self.directory]

        subprocess.call(command)
        print("Deciphered {}".format(self.directory))

    def cypher(self):
        command = ["umount", self.directory]
        subprocess.call(command)
        print("Cyphered {}".format(self.directory))

    @classmethod
    def prepare_directory(cls, password: str, directory_path: str):
        print("Preparing directory {}".format(directory_path))
        print("Used password: {}".format(password))


class AsyncCypher(Thread):
    """
    Purpose of this class is to provide interface for handling decipher asynchronously.
    Example usage(from the SecurityHandler class):
    async_decipher = AsyncDecipher(directory)
    async_decipher.start()
    """

    def __init__(self, callback, cypher: Cypher):
        self.callback = callback
        self.cypher = cypher
        super().__init__(target=self._handle_cypher)

    def _handle_cypher(self):
        self.cypher.cypher()
        self.callback()


def decipher_blocking(cypher: Cypher):
    cypher.decipher()
