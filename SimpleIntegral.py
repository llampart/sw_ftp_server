def summing_overflow_strategy(dt: float, data: list):
    return sum(map(lambda x: x * dt, data)) / dt


def no_overflow_strategy(*args):
    return 0


class TimedIntegral:
    """
    This class performs sort of integration of given signal
    In order for it to work properly, it have to be called with exactly dt seconds of interval
    """

    def __init__(self, fun: callable, dt: float, time_slots_span: int, overflow_strategy):
        self.fun = fun
        self.dt = dt
        self.time_slots_span = time_slots_span
        self.data_pointer = 0
        self.overflow_strategy = overflow_strategy
        self.history = []

    def __call__(self, *args, **kwargs):
        if self.data_pointer == self.time_slots_span:
            self.history[-1] = self.overflow_strategy(self.dt, self.history)
            self.history[0] = self.fun(*args, **kwargs)
            self.data_pointer = 1
        elif len(self.history) < self.time_slots_span:
            self.history.append(self.fun(*args, **kwargs))
            self.data_pointer += 1
        else:
            self.history[self.data_pointer] = self.fun(*args, **kwargs)
            self.data_pointer += 1
        return sum(map(lambda x: x, self.history)) * self.dt


def integrate(dt: float, time_slots_span: int, overflow_strategy=summing_overflow_strategy):
    def func_wrapper(fun):
        integral = TimedIntegral(fun, dt, time_slots_span, overflow_strategy)

        def class_method_wrapper(self, *args, **kwargs):
            return integral(self, *args, **kwargs)

        return class_method_wrapper

    return func_wrapper
